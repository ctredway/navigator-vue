import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';
import BootstrapVue from 'bootstrap-vue';
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap-vue/dist/bootstrap-vue.css';
import './assets/css/helper.css';
import './assets/css/main.css';
import { library } from '@fortawesome/fontawesome-svg-core';
import { faCaretLeft, faCaretDown, faLock, faCircle } from '@fortawesome/free-solid-svg-icons';
import { faCopy, faAngleDoubleLeft, faAngleDoubleRight, faPencilAlt } from '@fortawesome/free-solid-svg-icons';
import { faUpload, faDownload, faEdit, faTimes, faPlus, faTrashAlt, faCheck } from '@fortawesome/free-solid-svg-icons';
import { faClone } from '@fortawesome/free-regular-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';
import axios from 'axios';

library.add(faCaretLeft, faCaretDown, faLock, faCircle, faAngleDoubleLeft, faAngleDoubleRight, faCheck);
library.add(faClone, faCopy, faUpload, faDownload, faEdit, faTimes, faPlus, faTrashAlt, faPencilAlt);

Vue.config.productionTip = false;

Vue.use(BootstrapVue, axios);
Vue.component('b-badge', BootstrapVue.BBadge);
Vue.component('b-button', BootstrapVue.BButton);
Vue.component('b-card', BootstrapVue.BCard);
Vue.component('b-collapse', BootstrapVue.BCollapse);
Vue.component('font-awesome-icon', FontAwesomeIcon);

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount('#app');
